using EdsonIshizaka.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<AppDataContext>();

var app = builder.Build();

// POST : http://localhost:5034/api/funcionario/cadastrar
app.MapPost("/api/funcionario/cadastrar", ([FromBody] Funcionario funcionario, [FromServices] AppDataContext ctx) => {
    ctx.Funcionarios.Add(funcionario);
    ctx.SaveChanges();
    return Results.Created("", funcionario);
});


// GET : http://localhost:5034/api/funcionario/listar
app.MapGet("/api/funcionario/listar", ([FromServices] AppDataContext ctx) =>{

    if (ctx.Funcionarios.Any())
    {
        return Results.Ok(ctx.Funcionarios.ToList());
    }

    return Results.BadRequest("Nenhum funcionario encontrado");

});

// POST : http://localhost:5034/api/folha/cadastrar
app.MapPost("/api/folha/cadastrar/", ([FromBody] Folha folha, /*string ID*/ [FromServices] AppDataContext ctx) => {

    //Funcionario? funcionario = ctx.Funcionarios.FirstOrDefault(x => x.Id == id);

    ctx.Folhas.Add(folha);
    ctx.SaveChanges();
    return Results.Created("", folha);
});

// GET : http://localhost:5034/api/folha/listar
app.MapGet("/api/folha/listar", ([FromServices] AppDataContext ctx) =>{

    if (ctx.Folhas.Any())
    {
        return Results.Ok(ctx.Folhas.ToList());
    }

    return Results.BadRequest("Nenhum folha encontrada");

});

/*app.MapGet("api/folha/buscar/{cpf}/{mes}/{ano}", ([FromRoute] string cpf, string mes, string ano, [FromServices] AppDataContext ctx)=> {

    Funcionario funcionario = ctx.Funcionarios.Find(cpf);

    if (funcionario is true){
        Folha ano = ctx.Folhas.Find(ano);

        if (ano is true){
            Folha mes = ctx.Folhas.Find(mes);

            if (mes is true){
                
            }
        }
    }

});*/

app.Run();