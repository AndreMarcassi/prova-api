﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EdsonIshizaka.Migrations
{
    /// <inheritdoc />
    public partial class calculos : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Folhas_Folhas_folhasId",
                table: "Folhas");

            migrationBuilder.DropForeignKey(
                name: "FK_Funcionarios_Funcionarios_FuncionarioId",
                table: "Funcionarios");

            migrationBuilder.DropIndex(
                name: "IX_Funcionarios_FuncionarioId",
                table: "Funcionarios");

            migrationBuilder.DropIndex(
                name: "IX_Folhas_folhasId",
                table: "Folhas");

            migrationBuilder.DropColumn(
                name: "FuncionarioId",
                table: "Funcionarios");

            migrationBuilder.DropColumn(
                name: "folhasId",
                table: "Folhas");

            migrationBuilder.CreateTable(
                name: "calculos",
                columns: table => new
                {
                    Id = table.Column<string>(type: "TEXT", nullable: false),
                    SalarioBruto = table.Column<double>(type: "REAL", nullable: false),
                    ImpostoRenda = table.Column<double>(type: "REAL", nullable: false),
                    Inss = table.Column<double>(type: "REAL", nullable: false),
                    Fgts = table.Column<double>(type: "REAL", nullable: false),
                    salarioLiquido = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_calculos", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "calculos");

            migrationBuilder.AddColumn<string>(
                name: "FuncionarioId",
                table: "Funcionarios",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "folhasId",
                table: "Folhas",
                type: "TEXT",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Funcionarios_FuncionarioId",
                table: "Funcionarios",
                column: "FuncionarioId");

            migrationBuilder.CreateIndex(
                name: "IX_Folhas_folhasId",
                table: "Folhas",
                column: "folhasId");

            migrationBuilder.AddForeignKey(
                name: "FK_Folhas_Folhas_folhasId",
                table: "Folhas",
                column: "folhasId",
                principalTable: "Folhas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Funcionarios_Funcionarios_FuncionarioId",
                table: "Funcionarios",
                column: "FuncionarioId",
                principalTable: "Funcionarios",
                principalColumn: "Id");
        }
    }
}
