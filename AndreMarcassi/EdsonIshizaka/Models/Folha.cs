using System.ComponentModel.DataAnnotations.Schema;

namespace EdsonIshizaka.Models;

using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

public class Folha{

    public Folha (){
        Id = Guid.NewGuid().ToString();
    }

    public Folha(int quantidade, double valor, int mes, int ano){
        Quantidade = quantidade;
        Valor = valor;
        Mes = mes;
        Ano = ano;
        Id = Guid.NewGuid().ToString();
    }

    public string? Id { get; set; }
    public int? Quantidade { get; set; }
    public int Mes { get; set; }
    public double? Valor { get; set; }
    public int Ano { get; set; }

}