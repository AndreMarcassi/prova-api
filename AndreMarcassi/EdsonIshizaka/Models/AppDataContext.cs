using Microsoft.EntityFrameworkCore;
namespace EdsonIshizaka.Models;

public class AppDataContext: DbContext{

    public DbSet<Funcionario> Funcionarios { get; set; }
    public DbSet<Folha> Folhas { get; set; }

    public DbSet<Calculo> calculos{ get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=Andre_Edson.db");
    }

}