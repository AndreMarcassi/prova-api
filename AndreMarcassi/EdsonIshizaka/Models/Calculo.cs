using System.ComponentModel.DataAnnotations;

namespace EdsonIshizaka.Models;

public class Calculo
{
    public Calculo(){
      Id = Guid.NewGuid().ToString();
    }

    public Calculo (double salarioBruto, double impostoRenda , double inss , double fgts , double salarioLiquido){
        SalarioBruto = salarioBruto;
        ImpostoRenda = impostoRenda;
        Inss = inss;
        Fgts = fgts;
        SalarioBruto = salarioBruto;
        Id = Guid.NewGuid().ToString();
    }

    public void calcularInss(double salarioBruto){
      if(salarioBruto <  1693.72){
         Inss *= 0.08;
      }
      else if(salarioBruto >= 1693.73 && salarioBruto <= 2822.90){
         Inss *= 0.09;
      }
         else if(salarioBruto >= 2822.91 && salarioBruto <= 5645.80){
         Inss *= 0.11;
      }
         else if(salarioBruto >=  5645.81){
         Inss = 621.03;
      }     
   }

   public void calcularImpostoRenda(double salarioBruto){

      if(salarioBruto < 1903.98){
         ImpostoRenda *= 0.0;
      }
      else if(salarioBruto > 1903.99 && salarioBruto <= 2826.65){
         ImpostoRenda *= 0.075;
      }
         else if(salarioBruto >= 2826.66 && salarioBruto <= 3751.05){
         ImpostoRenda *= 0.15;
      }
         else if(salarioBruto >=  3751.06  && salarioBruto <= 4664.68){
         ImpostoRenda *= 0.225;
      }
         else if(salarioBruto >= 4664.69){
         ImpostoRenda *= 0.275;
      }
   }

   public void calcularFgts(double salarioBruto){
      Fgts = salarioBruto * 0.08;
   }

   public void calculosalarioLiquido(){
      salarioLiquido = (SalarioBruto - ImpostoRenda) - Inss;
   }

   public String Id { get; set; }
    public double SalarioBruto { get; set; }
    public double ImpostoRenda { get; set; }
    public double Inss { get; set; }
    public double Fgts { get; set; }
    public double salarioLiquido { get; set; }

    

}